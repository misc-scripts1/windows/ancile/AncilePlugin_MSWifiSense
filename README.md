# Ancile Microsoft WifiSense
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_MSWifiSense

Ancile Microsoft WifiSense plugin disables Microsoft's WifiSense service.

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile